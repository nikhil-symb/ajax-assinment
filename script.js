$(document).ready(function() {
    const flagSection = $('#flag-section');
    const searchTextInput = $('#input-box');

    $.ajax({
        url: 'https://restcountries.com/v3.1/all',
        method: 'GET',
        success: function(countries) {
            console.log(countries); 
            showCountries(countries);
            searchTextInput.on('input', function() {
                filterCountries(countries, searchTextInput.val());
            });
        },
        error: function(error) {
            console.error('Error fetching countries data:', error);
        }
    });


    function showCountries(countries) {
        flagSection.empty();
        countries.forEach(country => {
            const countryItem = $(`
                <div class="flag-div">
                    <img src="${country.flags.png}" alt="flag"/>
                    <div class="flag-info">
                        <h1>${country.name.common}</h1>
                        <p>Currency: ${country.currencies ? Object.values(country.currencies)[0].name : 'N/A'}</p>
                        <p>Current Date and Time: ${new Date().toLocaleString()}</p>
                        <div class="button-div">
                            <button onclick="showMap('${country.maps.googleMaps}')">Show Map</button>
                            <button onclick="showcountryDetails('${country.cca3}')">Show countryDetails</button>
                        </div>
                    </div>
                </div>
            `);
            flagSection.append(countryItem);
        });
    }

    function filterCountries(countries, searchTerm) {
        const filteredCountries = countries.filter(country => country.name.common.toLowerCase().includes(searchTerm.toLowerCase()));
        showCountries(filteredCountries);
    }
});

function showMap(mapUrl) {
    window.open(mapUrl, '_blank');
}

function showcountryDetails(countryCode) {
    window.location.href = `countryDetail.html?country=${countryCode}`;
}
