$(document).ready(function () {

    const urlParams = new URLSearchParams(window.location.search);
    const countryCode = urlParams.get('country');
    const countryDetailContainer = $('#country-detail');


    $.ajax({
        url: 'https://restcountries.com/v3.1/all',
        method: 'GET',
        success: function (countries) {

            const country = countries.find(country => country.cca3 === countryCode);

            displayCountryDetail(country, countries);
        },
        error: function (error) {
            console.error('Error fetching countries data:', error);
        }
    });


    function displayCountryDetail(country, allCountries) {
        console.log(country);


        countryDetailContainer.html(`
            <h1>${country.name.common}</h1>

            <div id="main-show-div">
                <div class="flag-image-picture">
                <img id="image-flag" src="${country.flags.png}" alt="Flag of ${country.name.common}">
                </div>
                <div id="information">
                <h2>Native name: ${country.name.nativeName ? Object.values(country.name.nativeName)[0].common : 'N/A'}</h2>
                <p>Capital:${country.capital ? country.capital.join(', ') : 'N/A'}</p>
                <p>Population: ${country.population.toLocaleString()}</p>  
                <p>Region: ${country.region}</p>
                <p>Subregion: ${country.subregion}</p>
                <p>Area: ${country.area.toLocaleString()} km²</p>
                <p>Country Code: ${country.idd.root}${country.idd.suffixes.length > 1 ? " " : country.idd.suffixes[0]}</p>
                <p>Languages: ${Object.values(country.languages).join(', ')}</p>
                <p>Timezones: ${country.timezones.join(', ')}</p>
                </div>
            </div>
            
            <div id="borders">Border Countries: <span id="border-countries"></span></div>
            <a href="index.html" class="back-button">Back to List</a>
        `);


        displayBorderCountries(country.borders, allCountries);
    }


    function displayBorderCountries(borders, allCountries) {
        const borderCountriesContainer = $('#border-countries');


        if (borders) {
            borders.forEach(borderCode => {

                const borderCountry = allCountries.find(country => country.cca3 === borderCode);
                if (borderCountry) {

                    const flagImg = $('<img >')
                        .attr('src', borderCountry.flags.png)
                        .attr('alt', `Flag of ${borderCountry.name.common}`)
                        .css({
                            width: '25%',
                            margin: '10px'
                        });

                    borderCountriesContainer.append(flagImg);
                }
            });
        } else {

        }
    }
});
